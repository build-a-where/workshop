package workshop

import (
	"testing"

	"codeberg.org/build-a-where/workshop/op"
)

func TestFilter(t *testing.T) {
	subject := func(groups ...Group) *Filter {
		return &Filter{groups: groups}
	}

	column := "id"
	operator := op.Equal
	value := 1

	t.Run("And()", func(t *testing.T) {
		t.Run("when the filter has no groups", func(t *testing.T) {
			filter := subject()

			result := filter.And(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with a single Group", func(t *testing.T) {
					if len(result.groups) != 1 {
						t.Errorf("Expected 1 Group, got %d", len(result.groups))
					}

					t.Run("with a single Condition", func(t *testing.T) {
						if len(result.groups[0]) != 1 {
							t.Errorf("Expected 1 Condition, got %d", len(result.groups[0]))
						}

						t.Run("with the correct column", func(t *testing.T) {
							if result.groups[0][0].Column != column {
								t.Errorf("Expected column %s, got %s", column, result.groups[0][0].Column)
							}
						})

						t.Run("with the correct operator", func(t *testing.T) {
							if result.groups[0][0].Op != operator {
								t.Errorf("Expected operator %v, got %v", operator, result.groups[0][0].Op)
							}
						})

						t.Run("with the correct value", func(t *testing.T) {
							if result.groups[0][0].Value != value {
								t.Errorf("Expected value %d, got %d", value, result.groups[0][0].Value)
							}
						})
					})
				})
			})
		})

		t.Run("when the filter has one group", func(t *testing.T) {
			column2 := "name"
			operator2 := op.Equal
			value2 := "John"

			group := Group{
				Condition{column2, operator2, value2},
			}

			filter := subject(group)

			result := filter.And(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with one Group", func(t *testing.T) {
					if len(result.groups) != 1 {
						t.Errorf("Expected 1 Groups, got %d", len(result.groups))
					}

					t.Run("with two Conditions", func(t *testing.T) {
						if len(result.groups[0]) != 2 {
							t.Errorf("Expected 2 Conditions, got %d", len(result.groups[0]))
						}

						t.Run("with the correct columns", func(t *testing.T) {
							if result.groups[0][0].Column != column2 {
								t.Errorf("Expected column %s, got %s", column2, result.groups[0][0].Column)
							}

							if result.groups[0][1].Column != column {
								t.Errorf("Expected column %s, got %s", column, result.groups[0][1].Column)
							}
						})

						t.Run("with the correct operators", func(t *testing.T) {
							if result.groups[0][0].Op != operator2 {
								t.Errorf("Expected operator %v, got %v", operator2, result.groups[0][0].Op)
							}

							if result.groups[0][1].Op != operator {
								t.Errorf("Expected operator %v, got %v", operator, result.groups[0][1].Op)
							}
						})

						t.Run("with the correct values", func(t *testing.T) {
							if result.groups[0][0].Value != value2 {
								t.Errorf("Expected value %s, got %s", value2, result.groups[0][0].Value)
							}

							if result.groups[0][1].Value != value {
								t.Errorf("Expected value %d, got %d", value, result.groups[0][1].Value)
							}
						})
					})
				})
			})
		})

		t.Run("when the filter has multiple groups", func(t *testing.T) {
			group1 := Group{
				Condition{"group1", op.Equal, true},
			}

			group2 := Group{
				Condition{"group2", op.Equal, true},
			}

			filter := subject(group1, group2)

			result := filter.And(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with the new Condition as the last item in the last pre-existing Group", func(t *testing.T) {
					if len(result.groups) != 2 {
						t.Errorf("Expected 2 Groups, got %d", len(result.groups))
					}

					if len(result.groups[1]) != 2 {
						t.Errorf("Expected 2 Conditions, got %d", len(result.groups[1]))
					}

					if result.groups[1][1].Column != column {
						t.Errorf("Expected column %s, got %s", column, result.groups[1][1].Column)
					}

					if result.groups[1][1].Op != operator {
						t.Errorf("Expected operator %v, got %v", operator, result.groups[1][1].Op)
					}

					if result.groups[1][1].Value != value {
						t.Errorf("Expected value %d, got %d", value, result.groups[1][1].Value)
					}
				})
			})
		})
	})

	t.Run("Or()", func(t *testing.T) {
		t.Run("when the filter has no groups", func(t *testing.T) {
			filter := subject()

			result := filter.Or(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with a single Group", func(t *testing.T) {
					if len(result.groups) != 1 {
						t.Errorf("Expected 1 Group, got %d", len(result.groups))
					}

					t.Run("with a single Condition", func(t *testing.T) {
						if len(result.groups[0]) != 1 {
							t.Errorf("Expected 1 Condition, got %d", len(result.groups[0]))
						}

						t.Run("with the correct column", func(t *testing.T) {
							if result.groups[0][0].Column != column {
								t.Errorf("Expected column %s, got %s", column, result.groups[0][0].Column)
							}
						})

						t.Run("with the correct operator", func(t *testing.T) {
							if result.groups[0][0].Op != operator {
								t.Errorf("Expected operator %v, got %v", operator, result.groups[0][0].Op)
							}
						})

						t.Run("with the correct value", func(t *testing.T) {
							if result.groups[0][0].Value != value {
								t.Errorf("Expected value %d, got %d", value, result.groups[0][0].Value)
							}
						})
					})
				})
			})
		})

		t.Run("when the filter has one group", func(t *testing.T) {
			group := Group{
				Condition{"group", op.Equal, true},
			}

			filter := subject(group)

			result := filter.Or(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with two Groups", func(t *testing.T) {
					if len(result.groups) != 2 {
						t.Errorf("Expected 2 Groups, got %d", len(result.groups))
					}

					t.Run("with the new Condition as the only item in the new Group", func(t *testing.T) {
						if len(result.groups[1]) != 1 {
							t.Errorf("Expected 1 Condition, got %d", len(result.groups[1]))
						}

						t.Run("with the correct column", func(t *testing.T) {
							if result.groups[1][0].Column != column {
								t.Errorf("Expected column %s, got %s", column, result.groups[1][0].Column)
							}
						})

						t.Run("with the correct operator", func(t *testing.T) {
							if result.groups[1][0].Op != operator {
								t.Errorf("Expected operator %v, got %v", operator, result.groups[1][0].Op)
							}
						})

						t.Run("with the correct value", func(t *testing.T) {
							if result.groups[1][0].Value != value {
								t.Errorf("Expected value %d, got %d", value, result.groups[1][0].Value)
							}
						})
					})
				})
			})
		})

		t.Run("when the filter has multiple groups", func(t *testing.T) {
			group1 := Group{
				Condition{"group1", op.Equal, true},
			}

			group2 := Group{
				Condition{"group2", op.Equal, true},
			}

			filter := subject(group1, group2)

			result := filter.Or(column, operator, value)

			t.Run("it returns a Filter", func(t *testing.T) {
				if result == nil {
					t.Error("Expected a Filter, got nil")
				}

				t.Run("with the new Condition as the only item in a new Group", func(t *testing.T) {
					if len(result.groups) != 3 {
						t.Errorf("Expected 3 Groups, got %d", len(result.groups))
					}

					if len(result.groups[2]) != 1 {
						t.Errorf("Expected 1 Condition, got %d", len(result.groups[2]))
					}

					if result.groups[2][0].Column != column {
						t.Errorf("Expected column %s, got %s", column, result.groups[2][0].Column)
					}

					if result.groups[2][0].Op != operator {
						t.Errorf("Expected operator %v, got %v", operator, result.groups[2][0].Op)
					}

					if result.groups[2][0].Value != value {
						t.Errorf("Expected value %d, got %d", value, result.groups[2][0].Value)
					}
				})
			})
		})
	})

	t.Run("Transform()", func(t *testing.T) {
		var received []Group
		expected := "it's more than meets the eye"

		transformer := func(groups []Group) []any {
			received = groups

			return []any{expected}
		}

		t.Run("when the filter has no groups", func(t *testing.T) {
			filter := subject()

			result := filter.Transform(transformer)

			t.Run("it applies the transformer to the filter's groups", func(t *testing.T) {
				if len(received) != len(filter.groups) {
					t.Errorf("Expected %d groups received, got %d", len(filter.groups), len(received))
				}

				if result[0] != expected {
					t.Errorf("Expected %s, got %s", expected, result[0])
				}
			})
		})
	})
}
