// package workshop provides a simple interface to build a where query for any
// database in a simple filter-style manner.
package workshop

import (
	"codeberg.org/build-a-where/workshop/op"
)

// Where creates a new Filter and adds a condition to it.
func Where(column string, o op.Op, value interface{}) *Filter {
	f := &Filter{}

	return f.And(column, o, value)
}
