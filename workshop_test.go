package workshop

import (
	"testing"

	"codeberg.org/build-a-where/workshop/op"
)

func TestWhere(t *testing.T) {
	column := "id"
	operator := op.Equal
	value := 1

	result := Where(column, operator, value)

	t.Run("it returns a Filter", func(t *testing.T) {
		if result == nil {
			t.Error("Expected a Filter, got nil")
		}

		t.Run("with a single Group", func(t *testing.T) {
			if len(result.groups) != 1 {
				t.Errorf("Expected 1 Group, got %d", len(result.groups))
			}

			t.Run("with a single Condition", func(t *testing.T) {
				if len(result.groups[0]) != 1 {
					t.Errorf("Expected 1 Condition, got %d", len(result.groups[0]))
				}

				t.Run("with the correct column", func(t *testing.T) {
					if result.groups[0][0].Column != column {
						t.Errorf("Expected column %s, got %s", column, result.groups[0][0].Column)
					}
				})

				t.Run("with the correct operator", func(t *testing.T) {
					if result.groups[0][0].Op != operator {
						t.Errorf("Expected operator %v, got %v", operator, result.groups[0][0].Op)
					}
				})

				t.Run("with the correct value", func(t *testing.T) {
					if result.groups[0][0].Value != value {
						t.Errorf("Expected value %d, got %d", value, result.groups[0][0].Value)
					}
				})
			})
		})
	})
}
