package workshop

import (
	"codeberg.org/build-a-where/workshop/op"
)

// Condition is a struct that holds a single condition for a query. It is
// made of a column, an operator and a value. For some operators, the value
// may be superfluous.
type Condition struct {
	Column string
	Op     op.Op
	Value  interface{}
}
