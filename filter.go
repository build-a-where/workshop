package workshop

import (
	"sync"

	"codeberg.org/build-a-where/workshop/op"
)

// Filter is a struct that holds the conditions for a query.
// It can be used to build a query for any database.
// It is thread-safe.
// It uses the builder pattern.
type Filter struct {
	locker sync.Mutex
	groups []Group
}

// And adds a condition to the filter.
// It is thread-safe.
// It uses the builder pattern.
// It returns the Filter itself.
// It always adds the condition to the last group.
// If there is no group, it creates one.
func (f *Filter) And(column string, o op.Op, value interface{}) *Filter {
	f.locker.Lock()
	defer f.locker.Unlock()

	return f.and(column, o, value)
}

// Or adds a condition to the filter.
// It is thread-safe.
// It uses the builder pattern.
// It returns the Filter itself.
// It always adds the condition to a new group.
func (f *Filter) Or(column string, o op.Op, value interface{}) *Filter {
	f.locker.Lock()
	defer f.locker.Unlock()

	if len(f.groups) == 0 {
		return f.and(column, o, value)
	}

	return f.or(column, o, value)
}

// Transform applies the given Transformer to the filter's groups and returns
// the final query as per the transformer's implementation.
// It is thread-safe.
func (f *Filter) Transform(t Transformer) []any {
	f.locker.Lock()
	defer f.locker.Unlock()

	return t(f.groups)
}

func (f *Filter) and(column string, o op.Op, value interface{}) *Filter {
	f.ensuregroups()
	f.ensureLastGroup()

	condition := Condition{
		Column: column,
		Op:     o,
		Value:  value,
	}

	idx := len(f.groups) - 1

	f.groups[idx] = append(f.groups[idx], condition)

	return f
}

func (f *Filter) or(column string, o op.Op, value interface{}) *Filter {
	f.ensuregroups()

	condition := Condition{
		Column: column,
		Op:     o,
		Value:  value,
	}

	f.groups = append(f.groups, Group{condition})

	return f
}

func (f *Filter) ensuregroups() {
	if f.groups == nil {
		f.groups = make([]Group, 0)
	}
}

func (f *Filter) ensureLastGroup() {
	if len(f.groups) == 0 {
		f.groups = append(f.groups, make(Group, 0))
	}
}
