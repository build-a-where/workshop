package workshop

// Group is a collection of conditions that have an AND relationship.
type Group []Condition
