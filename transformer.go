package workshop

// Transformer is a function that receives groups of conditions and returns
// the query and its values as they may be used in a prepared statement.
type Transformer func([]Group) []any
