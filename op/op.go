// package op defines the operators used in the where clause of a query.
package op

type Op uint

const (
	Equal Op = iota
	NotEqual
	GreaterThan
	LessThan
	GreaterOrEqual
	LessOrEqual
	Like
	In
	NotIn
	Null
	NotNull
)
